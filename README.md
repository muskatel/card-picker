# A random card picker

Picks a card from a deck of 52

## Table of Contents

- [Requirements](#requirements)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Requirements

Requires `cmake` and `gcc`.

## Usage

Uses Cmake
```sh
# Create build directory and move into it
$ mkdir -p build && cd build
# Generate makefile using Cmake - default is release
$ cmake ../
# Generate makefile for debug build
$ cmake ../ -DCMAKE_BUILD_TYPE=debug      
# Compile the project
$ make
#Run the compiled binary
$ ./main
```

## Maintainers

[Craig Marais (@muskatel)](https://gitlab.com/muskatel)

## Contributing

## License

---
Copyright 2022, Craig Marais ([@muskatel](https://gitlab.com/muskatel))
