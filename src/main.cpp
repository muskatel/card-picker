#include <iostream>
#include <vector>
#include <random>

//enum CARD_SUIT { Clubs, Diamonds, Hearts, Spades};
//enum CardValue {'2','3','4','5','6','7','8','9','T','J','Q','K','A'};

struct Card
{
  char faceValue;
  std::string suit;
};


int main()
{
  std::vector<Card> deck;
  deck.push_back({'2', "Clubs"});
  deck.push_back({'3', "Clubs"});
  deck.push_back({'4', "Clubs"});
  deck.push_back({'5', "Clubs"});
  deck.push_back({'6', "Clubs"});
  deck.push_back({'7', "Clubs"});
  deck.push_back({'8', "Clubs"});
  deck.push_back({'9', "Clubs"});
  deck.push_back({'T', "Clubs"});
  deck.push_back({'J', "Clubs"});
  deck.push_back({'Q', "Clubs"});
  deck.push_back({'K', "Clubs"});
  deck.push_back({'A', "Clubs"});

  deck.push_back({'2', "Diamonds"});
  deck.push_back({'3', "Diamonds"});
  deck.push_back({'4', "Diamonds"});
  deck.push_back({'5', "Diamonds"});
  deck.push_back({'6', "Diamonds"});
  deck.push_back({'7', "Diamonds"});
  deck.push_back({'8', "Diamonds"});
  deck.push_back({'9', "Diamonds"});
  deck.push_back({'T', "Diamonds"});
  deck.push_back({'J', "Diamonds"});
  deck.push_back({'Q', "Diamonds"});
  deck.push_back({'K', "Diamonds"});
  deck.push_back({'A', "Diamonds"});

  deck.push_back({'2', "Hearts"});
  deck.push_back({'3', "Hearts"});
  deck.push_back({'4', "Hearts"});
  deck.push_back({'5', "Hearts"});
  deck.push_back({'6', "Hearts"});
  deck.push_back({'7', "Hearts"});
  deck.push_back({'8', "Hearts"});
  deck.push_back({'9', "Hearts"});
  deck.push_back({'T', "Hearts"});
  deck.push_back({'J', "Hearts"});
  deck.push_back({'Q', "Hearts"});
  deck.push_back({'K', "Hearts"});
  deck.push_back({'A', "Hearts"});

  deck.push_back({'2', "Spades"});
  deck.push_back({'3', "Spades"});
  deck.push_back({'4', "Spades"});
  deck.push_back({'5', "Spades"});
  deck.push_back({'6', "Spades"});
  deck.push_back({'7', "Spades"});
  deck.push_back({'8', "Spades"});
  deck.push_back({'9', "Spades"});
  deck.push_back({'T', "Spades"});
  deck.push_back({'J', "Spades"});
  deck.push_back({'Q', "Spades"});
  deck.push_back({'K', "Spades"});
  deck.push_back({'A', "Spades"});

  // need to seed this with time
  std::default_random_engine generator;

  system("stty raw");
  while(true)
  {
    char input = getchar();
    if(input == 'q' || deck.size() == 0)
    {
      break;
    };

    system("clear"); // assuming unix



    // pick Card
    std::uniform_int_distribution<int> distribution (1, deck.size());
    int num = distribution(generator);

    Card card = deck.at(num);
    // display
    std::cout << " CARD: " << card.faceValue << " of "<< card.suit << std::endl;
    // remove card
    deck.erase(deck.begin() + num);

  };

  system("clear"); // assuming unix
  system("stty cooked"); // return terminal to normal!
  return 0;
}
